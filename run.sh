
. ./common.env

RAM=1024m
VERBOSE=0

opts="im:"
while getopts $opts opt
  do
  case $opt in
    i) useIndex=1 ;;
    m) RAM="$OPTARG" ;;
    \?) echo $USAGE " Error"
        exit;;
    esac
  done
shift `expr $OPTIND - 1`

XPROPS="-Xmx${RAM} -Xms${RAM}"

if [ $# -eq 0 ] ; then
  echo "ERROR: Missing data file"
  exit 1
  fi
PROPS="$PROPS -Dcli.dataFile=$1"

if [ -n "${useIndex}" ] ; then
  PROPS="$PROPS -Dcli.indexLoader=flatFileIndexLoader"
  fi
echo "PROPS=$PROPS"
echo "RAM=$RAM"

PGM=com.andre.lineserver.LineServerLauncher
time -p java $XPROPS -cp $CPATH $PROPS $PGM $* | tee log-server.txt

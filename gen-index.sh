
#
# Creates index file for specified data file. Index file name will have an appended ".idx".
#

. ./common.env

if [ $# -eq 0 ] ; then
  echo "ERROR: Missing data file"
  exit 1
  fi 
file=$1
PROPS="$PROPS -Dcli.dataFile=$1"

PGM=com.andre.lineserver.indexer.writer.IndexWriterDriver
time -p java $PROPS -cp $CPATH $PGM $* | tee log-gen-index.txt

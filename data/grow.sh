#/bin/bash

#
# grow.sh FILE HOW_MANY_TIMES_TO_APPEND
#

if [ $# -lt 2 ] ; then
  echo "ERROR: Missing file and growth count"
  exit 1
fi
file=$1
max=$2

if [ -f $file ] ; then
  echo "Growing file by $max times"
  bfile=big-$file
  cp $file $bfile
  ls -lh $file 
  for (( j=0; j<$max; j++ )) ; do
    echo " Copy $j "
    cat $file >> $bfile
    ls -lh $bfile 
  done
else
  echo "ERROR: File does not exist: $file"
  exit 1
fi

ls -l $file $bfile
ls -lh $file $bfile

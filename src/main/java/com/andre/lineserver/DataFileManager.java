package com.andre.lineserver;

import java.io.*;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;
import com.andre.lineserver.indexer.IndexManager;
import com.andre.lineserver.indexer.IndexRecord;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * Data file manager - retrieve a line from data file by line number.
 * The manager references two objects: a random-access data file and indexManager which indexes the file by line number.
 */
//@ManagedResource(objectName = "lineServer:name=DataFileManager")
public class DataFileManager {
	private final static Logger logger = Logger.getLogger(DataFileManager.class);
	private final AtomicLong callCount = new AtomicLong();
	private final File dataFile ;
	/** Data file */
	private final RandomAccessFile rafile ;
	/** Index for data file */
	private final IndexManager indexManager;

	public DataFileManager(File dataFile,  IndexManager indexManager) throws FileNotFoundException {
		this.rafile = new RandomAccessFile(dataFile,"r");
		this.dataFile = dataFile;
		this.indexManager = indexManager;
	}

	public String getLine(int lineNumber) throws Exception {
		IndexRecord record = indexManager.getIndex(lineNumber);
		logger.info(callCount+". lineNumber="+lineNumber+" record=["+record+"]");
		callCount.getAndIncrement();
		if (record == null)
			return null;
		rafile.seek(record.pos);
		byte [] bytes = new byte[record.length];
		rafile.read(bytes);
		return new String(bytes);
	}

	@ManagedAttribute(description = "Gets call count")
	public long getCallCount() {
		return callCount.get();
	}

	@ManagedAttribute(description = "Gets data file name")
	public String getDataFileName() {
		return dataFile.getPath();
	}

	@ManagedAttribute(description = "Gets data file length")
	public long getDataFileLength() {
		return dataFile.length();
	}

	@ManagedAttribute(description = "Gets indexManager class name")
	public String getIndexManagerClassName() {
		return indexManager.getClass().getName();
	}

	@ManagedAttribute(description = "Gets indexManager line count")
	public int getIndexManagerSize() {
		return indexManager.getLineCount();
	}


	@Override
	public String toString() {
		return
			"dataFile="+dataFile.getName()
			+" indexManager=["+indexManager+"]"
			;
	}
}

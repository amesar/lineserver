package com.andre.lineserver;

import java.util.concurrent.atomic.AtomicLong;
import org.apache.log4j.Logger;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.buffer.BigEndianHeapChannelBuffer;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.andre.metrics.MetricState;
import static com.codahale.metrics.MetricRegistry.name;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.JmxReporter;

/**
 * Handles requests for LineServer.
 */
//@ManagedResource(objectName = "lineServer:name=LineServerHandler")
public class LineServerHandler extends SimpleChannelHandler { 
	private final static Logger logger = Logger.getLogger(LineServerHandler.class);
	private final AtomicLong callCount = new AtomicLong();
	private final DataFileManager dataFileManager ;
	private final String END = "\r\n" ;
	private final String ERROR = "ERR";
	private final MetricRegistry metricRegistry = MetricState.REGISTRY;
	private final Timer timerGet;

	public LineServerHandler(DataFileManager dataFileManager) {
		this.dataFileManager = dataFileManager ;
		timerGet = metricRegistry.timer(name(LineServerHandler.class, "timerGet"));
		JmxReporter reporter = JmxReporter.forRegistry(metricRegistry).build();
		reporter.start();
	}

	@Override
	public void messageReceived(ChannelHandlerContext context, MessageEvent event) throws Exception { 
		//ChannelBuffer  buf = (ChannelBuffer) event.getMessage();
		callCount.getAndIncrement();
		BigEndianHeapChannelBuffer buf = (BigEndianHeapChannelBuffer) event.getMessage();
		int nbytes = buf.readableBytes();
		nbytes = nbytes-2;
		byte [] bytes = new byte[nbytes];
		buf.readBytes(bytes,0,nbytes);
		String line = new String(bytes);
		Channel channel = event.getChannel();
		processRequest(line, channel);
	}

	private void processRequest(String line, Channel channel) throws Exception {
		//logger.info("line="+line+". len="+line.length());
		Timer.Context context = timerGet.time();
		String [] toks = line.split(" ");
		String cmd = toks[0];
		//logger.info("cmd="+cmd+".");
		String response = null;
		try {
			if ("GET".equals(cmd)) {
				response = processGet(toks);
			} else if ("QUIT".equals(cmd)) {
				logger.info("QUIT");
				channel.close();
			} else if ("SHUTDOWN".equals(cmd)) {
				processShutdown(channel);
			} else {
				logger.error("Unknown command: "+cmd);
				response = ERROR;
			}
		} catch (Exception e) {
			logger.error("ERROR SERVER: "+e);
			response = ERROR;
		} finally {
			if (response != null) {
				ChannelFuture future = channel.write(response+END);
			}
			context.stop();
		} 
	}

	private String processGet(String [] toks) throws Exception {
		String content = null;
		if (toks.length < 2) {
			logger.error("Missing line number");
			content = ERROR;
		} else {
			int lineNumber = Integer.parseInt(toks[1]);
			content = dataFileManager.getLine(lineNumber);
			if (content == null) {
				content = ERROR;
			} else {
				content = "OK"+END+content;
			}
			//logger.info("content="+content+".");
		}
		return content;
	}

	private void processShutdown(Channel channel) {
		logger.info("SHUTDOWN");
		channel.close();
		Thread thread = new Thread(new MyRunnable(lineServer));
		thread.start();
	}

	class MyRunnable implements Runnable {
		LineServer lineServer ;
		MyRunnable(LineServer lineServer) {
			this.lineServer = lineServer;
		}

		public void run() {
			lineServer.shutdown();
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext context, ExceptionEvent event) { 
		logger.error("ERROR SERVER: "+event);
		event.getCause().printStackTrace();
		Channel ch = event.getChannel();
		ch.close();
	}

	private LineServer lineServer;
	public void setLineServer(LineServer lineServer) {
		this.lineServer = lineServer ;
	}

	@ManagedAttribute(description = "Gets call count")
	public long getCallCount() {
		return callCount.get();
	}

	@Override
	public String toString() {
		return 
			"dataFileManager=["+dataFileManager+"]"
			;
	}
}

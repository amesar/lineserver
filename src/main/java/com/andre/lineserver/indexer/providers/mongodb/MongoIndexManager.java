package com.andre.lineserver.indexer.providers.mongodb;

import com.mongodb.DBObject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.andre.lineserver.indexer.IndexManager;
import com.andre.lineserver.indexer.IndexRecord;
import com.andre.lineserver.indexer.providers.ProviderConfig;

/**
* Alternate MongoDB provider - under construction.
*/
public class MongoIndexManager implements IndexManager {
	private final MongoConnection conn ;

	public MongoIndexManager(MongoConnection conn) {
		this.conn = conn ;
	}

	public IndexRecord getIndex(int lineNumber) throws Exception  {
		BasicDBObject query = new BasicDBObject();
		query.put(ProviderConfig.COL_LINENUMBER, lineNumber);
		DBCursor cursor = conn.getCollection().find(query);
		if (cursor.count() == 0)
			return null ;
		DBObject dbobj = cursor.next();
		IndexRecord record = new IndexRecord(
			(Long)dbobj.get(ProviderConfig.COL_POS),
			(Integer)dbobj.get(ProviderConfig.COL_LENGTH));
		return record;
	}

	public int getLineCount() {
		return -1 ; // TODO
	}
}

package com.andre.lineserver.indexer.providers.jdbc;

import java.sql.*;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.andre.lineserver.indexer.IndexManager;
import com.andre.lineserver.indexer.IndexRecord;
import com.andre.lineserver.indexer.providers.ProviderConfig;

/**
 * Alternate JDBC provider - under construction.
 */
public class JdbcIndexManager extends JdbcDaoSupport implements IndexManager {
	private String table ;

	public JdbcIndexManager(String table) throws Exception {
		this.table = table ;
	}

	public IndexRecord getIndex(int key) throws Exception  {
		JdbcTemplate template = getJdbcTemplate();
		//String sql = "select * from "+table+ " where "+ProviderConfig.COL_LINENUMBER+"=?" ;  // TODO
		String sql = "select * from "+table+ " where "+ProviderConfig.COL_LINENUMBER+"="+key ;
		try {
			return (IndexRecord)template.queryForObject(sql,myRowMapper);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	private MyRowMapper myRowMapper = new MyRowMapper();

	class MyRowMapper implements RowMapper {
		public Object mapRow(ResultSet rs, int index) throws SQLException {
			IndexRecord record = new IndexRecord(rs.getInt(ProviderConfig.COL_POS), rs.getInt(ProviderConfig.COL_LENGTH));
			return record;
		}
	}

	public int getLineCount() {
		return -1 ; // TODO
	}

	@Override
	public String toString() {
		return
			"class="+this.getClass().getName()
			+" table="+table
			;
	}
}

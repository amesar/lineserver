package com.andre.lineserver.indexer.providers.flatfile;

import java.io.*;
import org.apache.log4j.Logger;
import com.andre.lineserver.indexer.DataFileIndexParser;
import com.andre.lineserver.indexer.writer.IndexWriter;
import com.andre.lineserver.indexer.providers.ProviderConfig;

/**
 * Loads index by reading from flat file where each record has following fields: lineNumber, filePosition, lineLength.
 */
public class FlatFileIndexWriter implements IndexWriter {
	private static final Logger logger = Logger.getLogger(FlatFileIndexWriter.class);
	private final DataFileIndexParser indexProcessor = new DataFileIndexParser();
	private int flushPeriod = 50000 ;

	public FlatFileIndexWriter() {
	}

	public void writeIndex(File dataFile) throws Exception {
		File indexFile = new File(dataFile.getPath()+".idx");
		logger.info("indexFile="+indexFile);
		
   		WriteHandler writeHandler = null;
		try {
			writeHandler = new WriteHandler(indexFile);
			indexProcessor.process(dataFile, writeHandler);
		} finally {
			if (null != writeHandler)
				writeHandler.close();
		}
	}

	public class WriteHandler implements DataFileIndexParser.LineHandler {
		private final PrintWriter writer ;
		WriteHandler(File file) throws IOException {
			this.writer = new PrintWriter(new FileOutputStream(file));
		}
		public void process(int lineNumber, long pos, int length) throws Exception {
			writer.println(lineNumber+ProviderConfig.FIELD_SEPARATOR +pos+ProviderConfig.FIELD_SEPARATOR +length);
            if (lineNumber % flushPeriod == 0) {
				//System.out.println("Flushing index file at line "+lineNumber);
				writer.flush();
			}
		}
		public void close() throws Exception {
			if (writer != null)
				writer.close();
		}
	}
 
	public int getFlushPeriod() { return flushPeriod; }
	public void setFlushPeriod(int flushPeriod) { this.flushPeriod = flushPeriod; } 
}

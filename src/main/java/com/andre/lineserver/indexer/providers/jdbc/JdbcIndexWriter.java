package com.andre.lineserver.indexer.providers.jdbc;

import java.io.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import com.andre.lineserver.indexer.DataFileIndexParser;
import com.andre.lineserver.indexer.writer.IndexWriter;

/**
 *  * Alternate JDBC provider - under construction.
 */
public class JdbcIndexWriter extends JdbcDaoSupport implements IndexWriter {
	private final String table ;
	private final DataFileIndexParser indexProcessor = new DataFileIndexParser();

	public JdbcIndexWriter(String table) {
        this.table = table;
	}

	public void writeIndex(File dataFile) throws Exception {
		JdbcTemplate template = getJdbcTemplate();
		template.execute("delete from "+table);
		indexProcessor.process(dataFile, new WriteHandler(template));
	}

	private static final String SQL = "INSERT INTO fileindex " + "(lineNumber, pos, length) VALUES (?, ?, ?)";

	public class WriteHandler implements DataFileIndexParser.LineHandler {
		private final JdbcTemplate template;
		WriteHandler(JdbcTemplate template) throws IOException {
			this.template = template;
		}
		public void process(int lineNumber, long pos, int length) throws Exception {
			template.update(SQL, new Object[] { lineNumber, pos, length });
		}
	}
}

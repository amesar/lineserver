package com.andre.lineserver.indexer.providers.flatfile;

import java.util.*;
import java.io.*;
import org.apache.log4j.Logger;
import com.andre.lineserver.indexer.IndexRecord;
import com.andre.lineserver.indexer.loader.IndexLoader;
import com.andre.lineserver.indexer.providers.ProviderConfig;

/**
 * Loads index by reading from flat file where each record has following fields: lineNumber, filePosition, lineLength.
 */
public class FlatFileIndexLoader implements IndexLoader {
	private final static Logger logger = Logger.getLogger(FlatFileIndexLoader.class);
	private final File indexFile ;
	private final int mod= 50000 ;

	public FlatFileIndexLoader(File indexFile) {
		this.indexFile = indexFile ;
	}

	public Map<Integer,IndexRecord> loadIndex() throws Exception {
		BufferedReader reader = null;
		try {
			Map<Integer,IndexRecord> map = new HashMap<Integer,IndexRecord>();
			reader = new BufferedReader(new FileReader(indexFile));
			String line ;
			int nlines=0;
			logger.info("indexFile="+indexFile);
			for ( ; (line = reader.readLine()) != null ; nlines++) {
				String [] toks = line.split(ProviderConfig.FIELD_SEPARATOR);
				if (toks.length != 3)
					throw new Exception("Expecting 3 fields at line "+nlines+" in file"+indexFile);
				Integer lineNumber = Integer.parseInt(toks[0]);
				if (nlines % mod == 0)
                	logger.info("Loading index "+nlines+" from "+indexFile);
				map.put(lineNumber,new IndexRecord( Long.parseLong(toks[1]), Integer.parseInt(toks[2])));
			}
			logger.info("indexFile="+indexFile+" lines="+nlines+" map="+map.size());
			return map;
		} finally {
			if (reader != null)
				reader.close();
		}
	}

	@Override
	public String toString() {
		return
			"class=="+this.getClass().getName()
			+" indexFile="+indexFile.getName()
			;
	}
}

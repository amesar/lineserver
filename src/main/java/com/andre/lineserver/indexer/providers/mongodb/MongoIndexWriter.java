package com.andre.lineserver.indexer.providers.mongodb;

import java.io.*;
import com.mongodb.DBObject;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;

import com.andre.lineserver.indexer.DataFileIndexParser;
import com.andre.lineserver.indexer.writer.IndexWriter;
import com.andre.lineserver.indexer.providers.ProviderConfig;

/**
 * Alternate MongoDB provider - under construction.
 */
public class MongoIndexWriter implements IndexWriter {
	private final String table = "fileindex";
	private final DataFileIndexParser indexProcessor = new DataFileIndexParser();
	private final MongoConnection conn ;

	public MongoIndexWriter(MongoConnection conn) {
		this.conn = conn;
	}

	public void writeIndex(File dataFile) throws Exception {
		indexProcessor.process(dataFile, new WriteHandler());
	}

	public class WriteHandler implements DataFileIndexParser.LineHandler {
		public void process(int lineNumber, long pos, int length) throws Exception {
			DBObject dbobj = new BasicDBObject();
			dbobj.put(ProviderConfig.COL_POS, pos);
			dbobj.put(ProviderConfig.COL_LENGTH, length);
			//WriteResult result = conn.getCollection().save(dbobj,writeConcern);
			WriteResult result = conn.getCollection().save(dbobj);   // TODO: check me
		}
	}
}

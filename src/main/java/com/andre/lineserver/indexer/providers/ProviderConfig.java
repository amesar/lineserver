package com.andre.lineserver.indexer.providers;

public class ProviderConfig {
	public final static String COL_LINENUMBER = "lineNum" ;
	public final static String COL_POS = "filePos" ;
	public final static String COL_LENGTH = "length" ;

	public final static String FIELD_SEPARATOR = " " ;
}

package com.andre.lineserver.indexer.loader;

import java.util.Map;
import com.andre.lineserver.indexer.IndexRecord;

/**
 * Loads an index into a map. Source can be a persisted index file or generated in-line from the data file itself.
 */
public interface IndexLoader {
	/**
	 * Loads an index into a map.
	 */
	public Map<Integer,IndexRecord> loadIndex() throws Exception ;
}

package com.andre.lineserver.indexer.loader;

import java.util.*;
import java.io.*;
import com.andre.lineserver.indexer.IndexRecord;
import com.andre.lineserver.indexer.DataFileIndexParser;

/**
 * Creates an index map by parsing data file.
 */
public class DataFileIndexLoader implements IndexLoader {
	private final File dataFile ;

	public DataFileIndexLoader(File dataFile) {
		this.dataFile = dataFile ;
	}

	public Map<Integer,IndexRecord> loadIndex() throws Exception {
		Map<Integer,IndexRecord> map = new HashMap<Integer,IndexRecord>();
		DataFileIndexParser indexProcessor = new DataFileIndexParser();
		indexProcessor.process(dataFile, new LineHandler(map));
		return map;
	}

	public class LineHandler implements DataFileIndexParser.LineHandler {
		Map<Integer,IndexRecord> map ;
		public LineHandler(Map<Integer,IndexRecord> map) {
			this.map = map;
		}
		public void process(int lineNumber, long pos, int length) throws Exception {
			map.put(lineNumber,new IndexRecord(pos, length));
		}
	}

	@Override
	public String toString() {
		return
			"class=="+this.getClass().getName()
			+" dataFile="+dataFile.getName()
			;
	}
}

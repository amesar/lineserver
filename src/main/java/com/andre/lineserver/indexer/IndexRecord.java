package com.andre.lineserver.indexer;

/**
 * Record for an index - contains file position and line length.
 */
public class IndexRecord {

	public IndexRecord(long pos, int length) {
		this.pos = pos;
		this.length = length;
	}

	/** File position. */
	public long pos;

	/** Line length. */
	public int length;

	@Override
	public String toString() {
		return "pos="+pos+" length="+length;
	}
}

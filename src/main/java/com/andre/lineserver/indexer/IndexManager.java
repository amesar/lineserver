package com.andre.lineserver.indexer;

import java.io.File;

/**
 * Manages indexes for data file. 
 */
public interface IndexManager {
	/** Returns file position and line length for a line number.  */
	public IndexRecord getIndex(int lineNumber) throws Exception ;

	/** Returns the number of lines in index. */
	public int getLineCount() ;
}

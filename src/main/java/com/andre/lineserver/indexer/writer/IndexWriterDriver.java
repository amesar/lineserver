package com.andre.lineserver.indexer.writer;

import java.io.*;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Invokes an IndexWriter that writes an index from a data file.
 */
public class IndexWriterDriver {
	private static final Logger logger = Logger.getLogger(IndexWriterDriver.class);

	public static void main(String[] args) throws Exception {
		new IndexWriterDriver().process(args);
	}

	public void process(String [] args) throws Exception {
		if (args.length < 1) {
			System.out.println("ERROR: Missing data file name");
			return;
		}
		String beanName = "indexWriter" ;
		if (args.length > 1) {
			beanName = args[1];
		}

		File file = new File(args[0]);
		ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");
		IndexWriter writer = context.getBean(beanName,IndexWriter.class);
		logger.info("writer="+writer.getClass().getName());
		logger.info("file="+file);
		writer.writeIndex(file);
	}
}

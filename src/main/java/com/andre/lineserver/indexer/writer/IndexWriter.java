package com.andre.lineserver.indexer.writer;

import java.io.File;

/**
 * Writes an index from a data file.
 */
public interface IndexWriter {
	public void writeIndex(File dataFile) throws Exception ;
}

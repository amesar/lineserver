package com.andre.lineserver.indexer;

import java.util.*;
import java.io.*;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import com.andre.lineserver.indexer.loader.IndexLoader;

/**
 * In memory index manager.
 */
//@ManagedResource(objectName = "lineServer:name=RamIndexManager") 
public class RamIndexManager implements IndexManager {
	private final Map<Integer,IndexRecord> map ;
	private final IndexLoader indexLoader ;

	public RamIndexManager(IndexLoader indexLoader) throws Exception {
		this.indexLoader = indexLoader ;
		map = indexLoader.loadIndex();
	}

	public IndexRecord getIndex(int key) throws Exception  {
		return map.get(key);
	}

	@ManagedAttribute(description = "Gets line count in index")
	public int getLineCount() {
		return map.size();
	}

	@ManagedAttribute(description = "Gets index class name")
	public String getIndexLoaderClassName() {
		return indexLoader.getClass().getName();
	}

	@Override
	public String toString() {
		return
			"class="+this.getClass().getName()
			+" map.size="+map.size()+""
			+" indexLoader=["+indexLoader+"]"
			;
	}
}

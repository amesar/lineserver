package com.andre.lineserver.indexer;

import java.util.*;
import java.io.*;

/**
 * Parses a data file and creates index record for each line. 
 * The callback LineHandler contains logic to be performed for each index record, e.g. either load into memory or persist.
 */
public class DataFileIndexParser {
	private final static int NL_TWEAK = 1;
	private final int mod = 50000;

	public int process(File dataFile, LineHandler lineHandler) throws Exception {
		BufferedReader reader = null ;
		System.out.println("Indexing file "+dataFile+" lineHandler="+lineHandler.getClass().getName());
		try {
			reader = new BufferedReader(new FileReader(dataFile));
			String line ;
			int lineNumber=1 ; 
			long startTime = System.currentTimeMillis();
			long priorPos = -1;
			long priorTime = -1;
			for (long pos=0 ; (line = reader.readLine()) != null ; lineNumber++ ) {
				int length = line.length();
				lineHandler.process(lineNumber, pos, line.length()) ;
				pos += length+NL_TWEAK;
				if (lineNumber % mod == 0) { // always nice to see details as to what's going on
					long bytes = pos-priorPos ;
					long ms = System.currentTimeMillis()-priorTime ;
					long tput = ms==0 ? -1 : bytes / ms ;
					System.out.println("Indexing: line="+lineNumber+" pos="+pos+" length="+length+
						(priorPos==-1 ? "" : " bytes="+(pos-priorPos))+" ms="+ms+" bytes/ms="+tput);
					priorPos = pos ;
					priorTime = System.currentTimeMillis();
				}
			}
			long totalTime = System.currentTimeMillis()-startTime;
			long tput = totalTime==0 ? -1 : dataFile.length() / totalTime ;
			System.out.println("Indexed: lines="+lineNumber+" bytes="+dataFile.length()+" ms="+totalTime+" bytes/ms="+tput+" handler="+lineHandler.getClass().getName());
			return lineNumber;
		} finally {
			if (reader != null)
				reader.close();
		}
	}

	/**
	 * Action to be performed for each line.
	 */
	public interface LineHandler {
		public void process(int lineNumber, long pos, int length) throws Exception ;
	}
}

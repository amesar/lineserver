package com.andre.lineserver.client;

import java.io.*;
import java.net.Socket;
import org.apache.log4j.Logger;

/**
 * A quick 'n dirty black/white service client. TODO to enhance.
 */
public class LineServerClient {
	private static final Logger logger = Logger.getLogger(LineServerClient.class);
	private final int port = 10497 ;
	private final String address = "localhost";
	private final Socket socket ;
	private final String CR = "\r";
	private final String NL = "\n";

	public LineServerClient(String address, int port) throws Exception {
		socket = new Socket(address, port);
	}

	/** 
	 *Returns a line for specified line number. 
	 **/
	public String getLine(int lineNumber) throws Exception {
		return call("GET "+lineNumber);
	}

	/** 
     * Returns server response for given request. 
	 */
	public String call(String request) throws Exception {
		PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
   		BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		request += NL;
		logger.debug("request="+request);
		output.println(request);

	   	String line = input.readLine();
		if (line != null)
			logger.debug("line: length="+line.length()+" content="+line);
		if (line == null) {
			return null; // DISCONNECTED
		}

		// TODO: check out better way to do this.
	   	StringBuilder response = new StringBuilder(line);
		if ("OK".startsWith(line)) {
	   		line = input.readLine();
	   		response.append(CR+NL+line);
		}
   		response.append(CR+NL);

		logger.debug("response: length="+response.length()+" content="+response);
		return response.toString();
	}

	public void close() throws IOException {
		if (socket != null) socket.close();
	}
}

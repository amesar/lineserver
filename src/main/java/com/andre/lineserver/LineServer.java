package com.andre.lineserver;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import org.apache.log4j.Logger;

import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.codec.string.StringDecoder;
import org.jboss.netty.handler.codec.string.StringEncoder;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * LineServer server.
 */
//@ManagedResource(objectName = "lineServer:name=LineServer")
public class LineServer {
	private static final Logger logger = Logger.getLogger(LineServer.class);
	private final int port;
	private final LineServerHandler handler ;
	private final ChannelGroup allChannels = new DefaultChannelGroup("LineServer");
	private ChannelFactory factory ;

	public LineServer(LineServerHandler handler, int port) throws Exception {
		this.handler = handler;
		handler.setLineServer(this);
		this.port = port;
	}

	public void run() {
		ExecutorService bossPool = Executors.newCachedThreadPool();
		ExecutorService workerPool = Executors.newCachedThreadPool();
		factory = new NioServerSocketChannelFactory(bossPool,workerPool);
		ServerBootstrap bootstrap = new ServerBootstrap(factory);

		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
			public ChannelPipeline getPipeline() throws Exception {
				ChannelPipeline pipeline = Channels.pipeline(handler);
				pipeline.addLast("decoder", new StringDecoder());
				pipeline.addLast("encoder", new StringEncoder());
				return pipeline ;
			}
		});

		Channel channel = bootstrap.bind(new InetSocketAddress(port));
 		allChannels.add(channel);
	}

	public void shutdown() {
		ChannelGroupFuture future = allChannels.close();
   		future.awaitUninterruptibly();
		factory.releaseExternalResources();
		logger.debug("shutdown server");
	}

	@ManagedAttribute(description = "Gets port")
	public int getPort() {
		return port;
	}
}

package com.andre.lineserver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Launches the LineServer.
 */
public class LineServerLauncher {
	public static void main(String[] args) throws Exception {
		new LineServerLauncher().process(args) ;
	}

	private void process(String[] args) throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");
		LineServer server = context.getBean("lineServer",LineServer.class);
		server.run();
	}
}

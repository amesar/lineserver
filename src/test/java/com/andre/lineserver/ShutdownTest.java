package com.andre.lineserver;

import java.util.*;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.*;
import com.andre.lineserver.client.LineServerClient;

/**
 * Tests SHUTDOWN request.
 * Works OK against standalone server - but does not work against embedded server.
 * TODO research.
 */
public class ShutdownTest extends BaseTest {
	private final static Logger logger = Logger.getLogger(ShutdownTest.class);
	private LineServerClient client ;

	@Test
	public void testShutdown() throws Exception {
		LineServerClient client = new LineServerClient(address,port) ;
		String response = client.call("SHUTDOWN");
		logger.debug(response);
		Assert.assertNull(response);
		response = client.call("GET 1");
		logger.debug(response);
		Assert.assertNull(response);
	}
}

package com.andre.lineserver;

import org.testng.annotations.*;
import com.andre.lineserver.client.LineServerClient;

/**
 * Tests GET request.
 */
public class GetTest extends BaseTest {
	private LineServerClient client ;

	@DataProvider(name = "legalLineNumbers")
	public Object[][] legalLineNumbers() {
		return new Object[][] {
			{ 1 },
			{ lineCount }
		};
	}

	@DataProvider(name = "illegalLineNumbers")
	public Object[][] illegalLineNumbers() {
		return new Object[][] {
			{ -1 },
			{ 0 },
			{ lineCount+1 },
			{ Integer.MAX_VALUE }
		};
	}

	@DataProvider(name = "badRequests")
	public Object[][] badRequests() {
		return new Object[][] {
			{ "GET" },
			{ "GET XX" },
			{ "FOO" },
			{ "1@#$%^&*()_+" },
		};
	}

	@BeforeClass
	public void beforeClass() throws Exception {
		client = new LineServerClient(address,port) ;
	}

	@AfterClass
	public void afterClass() throws Exception {
		client.close();
	}

	@Test(dataProvider = "legalLineNumbers")
	public void testLegalLineNumber(int lineNumber) throws Exception {
		assertOk(client.getLine(lineNumber));
	}

	@Test(dataProvider = "illegalLineNumbers")
	public void testIllegalLineNumber(int lineNumber) throws Exception {
		assertErr(client.call(client.getLine(lineNumber)));
	}

	@Test(dataProvider = "badRequests")
	public void testBadRequest(String request) throws Exception {
		assertErr(client.call(client.call(request)));
	}
}

package com.andre.lineserver;

import org.testng.Assert;
import org.testng.annotations.*;
import com.andre.lineserver.client.LineServerClient;

/**
 * Tests QUIT request.
 */
public class QuitTest extends BaseTest {
	private LineServerClient client ;

	@BeforeClass
	public void beforeClass() throws Exception {
		client = new LineServerClient(address,port) ;
	}

	@AfterClass
	public void afterClass() throws Exception {
		client.close();
	}

	@Test
	public void testQuit() throws Exception {
		String response = client.call("QUIT");
		Assert.assertNull(response);
		response = client.call("GET 1");
		Assert.assertNull(response);
	}
}

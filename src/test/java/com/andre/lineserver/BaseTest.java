package com.andre.lineserver;

import java.io.*;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.*;
import com.andre.lineserver.indexer.loader.DataFileIndexLoader;
import com.andre.lineserver.indexer.loader.IndexLoader;
import com.andre.lineserver.indexer.IndexManager;
import com.andre.lineserver.indexer.RamIndexManager;

/**
 * Base class for tests.
 */
public class BaseTest {
	private final static Logger logger = Logger.getLogger(BaseTest.class);
	static private LineServer server ;
	static String address = "localhost" ; 
	static int port = 10497 ;
	final static String OK = "OK\r\n";
	final static String ERR = "ERR\r\n";
	final static File dataFile = new File("data/test.txt");
	static int lineCount;

	@BeforeSuite
	public void beforeSuite() throws Exception {
		IndexLoader indexLoader = new DataFileIndexLoader(dataFile);
		IndexManager indexManager = new RamIndexManager(indexLoader);
		DataFileManager dataFileManager = new DataFileManager(dataFile,indexManager);
		lineCount = indexManager.getLineCount();
		LineServerHandler handler = new LineServerHandler(dataFileManager);

		server = new LineServer(handler,port);
		server.run();
	}

	void assertOk(String response) {
		Assert.assertTrue(response != null && response.startsWith(OK));
	}

	void assertErr(String response) {
		Assert.assertTrue(response != null && response.equals(ERR));
	}
}


# LineServer Project

## Relative Links Note

Note: bitbucket Markdown does NOT support relative links from its public site as does github.

See [Issue #6589](https://bitbucket.org/site/master/issue/6589/markdown-relative-link-to-image-and-other) _Markdown relative link to image and other readme does not work_.

In order to see links correctly you will have to clone the project to your local machine and use a browser Markdown plugin such as Markdown Viewer.
```
git clone https://amesar@bitbucket.org/amesar/lineserver.git
```

## Overview

[REQUIREMENTS.md](REQUIREMENTS.md) - Requirements for the project.

## Building and Running

### Building the application

To build the project run the build.sh script which invokes maven.

    build.sh

or

	mvn clean install

You can then see [the test results](target/surefire-reports/html/index.html) - or see this [sample](doc/testng-report/index.html).

By default build.sh runs the tests - if you wish to skip the tests:

	mvn -DskipTests=true clean install


### Running the application

#### Run the server

	run.sh data/medium-logs.txt

#### Data files

The [data](data) directory contains sample data files.

In order to test large/huge files, see the script "grow.sh $FILE $COUNT" which grows the specified file by COUNT times. 

For example, if discourse-on-the-method.txt is 128664 bytes, the following command will create
a file big-discourse-on-the-method.txt whose size is 514656 bytes.

	grow.sh discourse-on-the-method.txt 3

#### Run server with index file

run.sh will generate the index every time from the data file. 
If you wish to run with a pre-built index file, first build the index file once:

	build-index.sh data/medium-logs.txt

This will create the index file data/medium-logs.txt.idx.
Then use the -i option for run.sh which will read the index file first:

	run.sh -i data/medium-logs.txt   

#### Run client 

Use telnet client to invoke the server:

	telnet localhost 10497


## Implementation 

### Architecture Overview

![Architecture Diagram](doc/architecture.png)

### Core Classes

* LineServer - Netty Server entry point.
* LineServerHandler - Handles requests to LineServer.
* DataFileManager - Retrieves content from data file for a line number.
* RamIndexManager - In-memory index manager using Java map - retrieves index record for line number.
* IndexLoader - Loads an index into an in-memory Java map.
  * DataFileIndexLoader - Parses data file in-line and loads index. 
  * FlatFileIndexLoader - Reads index from external flat file.  
 Cost is amortized across startup. To create index see [gen-index.sh](gen-index.sh).

### Architecture Details

* Basic implementation is to create an in-memory index as a Java map (via IndexLoader). The line number is the key and the value is a tuple of file position and line length.
* We open the data file as a Java random access file, and simply seek to file position per desired line number.
* There are two ways to load an index:
  * Load the index on the fly by parsing the data file at start up time.
  * Load the index by reading a flat file containing the index data (line number, file position and line length).

* The basic implementation is constrained by the size of the index which must fit into memory.

* Experimental:
  * An quickie dive was done into alternate index providers - JDBC and MongoDB.
  * Of course, this would be slower, but it would be more scalable.
  * Other more scalable index providers would be external cache servers such as Memcached, EhCache or Redis.


## Answers to Questions

How does your system work? (if not addressed in comments in source)

* See above

How will your system perform as the number of requests per second increases?

* Performance will slow down because as more requests come in, seek time will be the main constraining factor
as the disk head will be moving around depending on which file position is requested.
Of course, transparent OS file page caching will mitigate this to the degree that memory is available but this
is obviously constrained by the memory requirements of the index.

How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file?

* The primary requirement is for the index to fit into main memory. 
The size of the index is related to the average line length. 
A large file with many short lines will generate a much larger index than the one with fewer long lines.
* The size of the data file itself is not an issue since only the requested line is loaded into memory.
* Since the index uses a Java map, the size of the map is limited by the size of an integer.
* Since the data file is randomly accessed, it does not have to fit into memory.
* With 7.5 GB memory (per spec), 1 GB file is OK. 10 GB file performance would depend upon the size of the index. 
100 GB would be problematic as the index most probably won't fit into RAM.
* Other things to explore:
  * Java 7 new File package. I was constrained to use 6.
  * Check out NIO package, i.e.  java.nio.MappedByteBuffer
  * Explore how other folks handle large files
  * Investigate memory mapped files

What documentation, websites, papers, etc did you consult in doing this assignment?

* Netty's website and some info on socket programming
* Did a bit of searching about Java large files 
* Leveraged some past experience with manipulating random files

What third-party libraries or other tools does the system use?

* [Netty 3.3.1](http://netty.io/) for erver implementation
* [Spring 3.2.4](http://projects.spring.io/spring-framework/) for dependency injection
* [Codahale Metrics 3.0.1](http://metrics.codahale.com) for cool metrics exposed via JMX
* [TestNG 6.8.7](http://testng.org/) for testing
* maven for building

How long did you spend on this exercise?

* 3 hours to get the basic server working per spec.
* 4 hours to 
  * Refactor to create more extensible and pluggable architecture with interfaces
  * Add basic testing 
  * Explore some future directions by create alternate IndexManager implementations such as JdbcIndexManager, MongoIndexManager, etc.
* 2 hours to document.
* Ongoing performance testing - testing at scale takes time :)

## Testing
* Though not part of challenge, I put together a few basic tests with an embedded Netty server.
* See [sample TestNG report](doc/testng-report/index.html).
* After building application, see [TestNG report](target/surefire-reports/html/index.html).
* Test coverage:
  * Test for correctness of GET
  * Test for bad input of GET 
  * Test for bad input in general
  * Test that QUIT closes the connection
  * Test that SHUTDOWN closes the server. Note, does so correctly for standalone server, but embedded server didn't behave as expected - research needed.
* Used the venerable TestNG toolkit whose following features provided a productivity boost:
  * DataProvider concept to declaratively invoke parameterized test methods - for example see [Get.java](src/test/java/com/andre/lineserver/GetTest.java)
  * @TestSuite annotation for suite wide fixture
  * Ordering of tests specified in testng.xml file to correctly execute ordered tests (e.g. Shutdown is the last test)

## Performance and Scalability

Tested on 

* 2.5 GHz Intel Core i7 MacBook Pro with 8 GB RAM 
* Amazon EC2 m1.xlarge instance with 16 GB

Baseline latency for one request is .4 ms for Mac. 
EC2 has a rather surprising high 3 ms - both client and server in same data center or even on same machine.

### Scaling the Index

As mentioned above, the key scalability constraint is that the index fit into memory with the main implementation.
As a potential solution I did a first pass on implementing a JDBC and MongoDB implementation of IndexReader.
These approaches would address scalability at the cost of performance. 

Since MongoDB uses memory mapped files, it will consume all available memory, and when none is available it will 
use disk. The index file would live on the the other disk partition specified in the requirements.

The JDBC solution would also allow for scale.
The current JdbcIndexWriter implementation is a primitive first pass. Better solution is to use batch inserts or the best
approach would be to create a file with SQL insert(s) and use MySQL import (much faster).

Some other potential solutions:

* Use java.nio.MappedByteBuffer for the index
* For a memory-based approach, use an additional index for the index - this would allow for larger indexes but still be constrained by memory.
* Look at alternative persistent stores such as Redis.
* Use an external cache such as Memcached to front a persistent solution - not necessary for Redis.

### Large Data Files

Some large data files tested - basic 7 GB and 110 GB file. Need to test some more in-between sizes such as 20 GB, 30 GB, etc.

	File                     Bytes            Lines
	huge-esapi-logs.txt      110,207,811,735  160,650,000
	huge-esapi-logs.txt.idx    4,062,307,334  160,650,000
	big-esapi-logs.txt         7,347,187,449   10,710,000
	big-esapi-logs.txt.idx       244,172,300   10,710,000

### Sample Load Test Results

Some sample results from [vtest](http://amesar.wordpress.com/2010/04/12/vtest-testing-framework/) load testing framework.
The context is a 1 GB data file with both server and client running on Mac laptop. 

Obviously next step would be to run test on EC2.
```
Thr      Test  Req/Sec  Total   Mean   50.0%   90.0%   99.0%   99.5%   99.9%     Max  Errs  Fails  StdDev
  1   GetLine  2686.80   3030   0.36       0       1       1       1       7      25    19      1    0.67
  2   GetLine  3833.62   1761   0.50       0       1       1       3      12      39    17      4    1.19
  5   GetLine  4133.43   1971   1.18       1       1       3       9     164     372    24      0   10.53
 10   GetLine  5535.33   1472   1.77       1       2       8      14      95     118    25      1    5.20
 20   GetLine  5120.05   1591   3.87       2       6      22      34     424     429    25      3   15.68
 50   GetLine  5549.86   1464   8.67       6      16      74     101     128     132    21      2   12.02
```
### Issues

Testing at scale takes time. 
Not only is time necessary to baby sit long-running tests, but every tweak to fix a problem, can necessitate a re-run of the test.

The 7 GB test file ran OK but when I tried the 100 GB I ran into mysterious problems.
I tried various -Xmx sizes (4096m to 14336m), but the JVM was exiting without any error - no OutOfMemory exception or other
diagnostic.
Need to do more research on this.

### JConsole 

I've also used the cool Codahale Metrics package to display quite performance metrics via JMX.
In the example below, the are 211,595 calls to the server with a mean time of 0.87 ms.

![JConsole](doc/jconsole-LineServer.png)

## Other

* Distribution - a tarball distro is generated with maven-assembly-plugin in target/lineserver-distribution.tar.gz - expanded directory is target/lineserver-distribution.
See [src/assembly/distribution.xml](src/assembly/distribution.xml).

## Issues

While testing a 100 GB data file at the last, I came accross a subtle Spring bug/issue relating to Spring's JMX @ManagedResource
annotation. Apparently when you annotate a class with @ManagedResource, Spring will eagerly load that class through
the application context and ignore prototype scope. Marking the class with lazy load, would have forced me to add
no-arg constructors, something that would have broken the "good practice" of avoiding no-arg constructors when not
necessary. Besides, there wasn't time at the last moment to grok all this. Solution was to remove @ManagedResource from
culprit classes.

## TODOs

* Brush up on raw socket programming
* Dive deeper into Netty especially regarding socket connection pooling
* Add option to run tests against an already running server instead of launching embedded server.
* Describe data/grow.sh
* Testing - lots of enhancements possible
  * Add option to test against an existing external server instead of just embedded. 
This would simply be adding an external property that would skip the launching of the embedded server.
  * Instead of using fixed data file, create one on the fly using java.io.File.createTempFile().

